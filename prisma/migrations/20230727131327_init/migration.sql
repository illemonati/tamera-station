-- CreateTable
CREATE TABLE "RTSPCameraSource" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "url" TEXT NOT NULL,
    "protocol" TEXT NOT NULL,
    "name" TEXT
);
