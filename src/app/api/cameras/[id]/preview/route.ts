import { getPreview } from "@/stream-receive";
import { getRTSPCameaSource } from "@/utils";
import { NextRequest, NextResponse } from "next/server";
import fs from "fs";
import { Readable } from "stream";

export const GET = async (
    req: NextRequest,
    { params }: { params: { id: string } }
) => {
    const { id } = params;

    const source = await getRTSPCameaSource(id);

    const image = await getPreview(
        source.url,
        //@ts-ignore
        source.protocol,
        source.control
    );

    return new NextResponse(image as unknown as ReadableStream, {
        headers: {
            "Content-Type": "image/png",
        },
    });
};
