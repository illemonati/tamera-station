import { addCameraToDB } from "@/stream-receive";
import { NextRequest, NextResponse } from "next/server";

export const POST = async (req: NextRequest) => {
    const { url, protocol, control } = await req.json();
    try {
        const camera = await addCameraToDB(url, protocol, control);
        return NextResponse.json(camera);
    } catch (e) {
        console.error(e);
        return NextResponse.json({ error: "Database Error" }, { status: 500 });
    }
};
