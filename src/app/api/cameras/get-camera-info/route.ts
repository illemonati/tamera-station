import { connectToRTSPEndPoint } from "@/stream-receive";
import { NextRequest, NextResponse } from "next/server";

export const POST = async (req: NextRequest) => {
    const { url, protocol } = await req.json();
    try {
        const { detailsArray } = await connectToRTSPEndPoint(url, protocol);
        return NextResponse.json(detailsArray);
    } catch (e) {
        console.error(e);
        return NextResponse.json(
            { error: "Connection Error" },
            { status: 500 }
        );
    }
};
