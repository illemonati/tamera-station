import { prisma } from "@/server/prisma";
import { NextRequest, NextResponse } from "next/server";

export const GET = async (req: NextRequest) => {
    try {
        const cameras = await prisma.rTSPCameraSource.findMany();
        return NextResponse.json(cameras);
    } catch (e) {
        console.error(e);
        return NextResponse.json({ error: "Database Error" }, { status: 500 });
    }
};
