import { TAMRA_PASSWORD } from "@/config";
import { cookies } from "next/dist/client/components/headers";
import { NextRequest, NextResponse } from "next/server";

export async function POST(req: NextRequest) {
    return NextResponse.json({
        valid: req.cookies.get('password')?.value === TAMRA_PASSWORD,
    });
};


export async function GET(req: NextRequest) {
    let valid = false;
    return NextResponse.json({
        valid,
    });
};
