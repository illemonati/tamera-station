"use client";

import { useCameras } from "@/client-utils";
import { Box, Container, Fab, Paper, Typography } from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";
import { RTSPCameraSource } from "@prisma/client";
import { useEffect, useRef } from "react";
import HomeIcon from "@mui/icons-material/Home";
import Link from "next/link";

const CameraViewPage = ({ params }: { params: { id: string } }) => {
    const { id } = params;

    const videoRef = useRef<HTMLVideoElement>(null);

    const websocketURL = new URL(
        `/bapi/live-stream/stream-ws?id=${id}`,
        window && window.location.href
    );
    websocketURL.protocol = websocketURL.protocol.replace("http", "ws");

    const { cameras, isLoading, isError } = useCameras();
    const thisCamera: RTSPCameraSource | null =
        isLoading || isError
            ? null
            : cameras.find((camera) => camera.id === id) || null;

    useEffect(() => {
        if (!videoRef.current) return;

        const mediaSource = new MediaSource();
        const dataChunks: Uint8Array[] = [];
        let sourceBuffer: SourceBuffer | null = null;

        videoRef.current.src = URL.createObjectURL(mediaSource);

        const socket = new WebSocket(websocketURL.href);
        socket.binaryType = "arraybuffer";
        socket.addEventListener("message", (e) => {
            dataChunks.push(new Uint8Array(e.data));
            appendBuffer();
        });

        const appendBuffer = () => {
            if (
                sourceBuffer &&
                !sourceBuffer.updating &&
                dataChunks.length > 0
            ) {
                const data = dataChunks.shift();
                data && sourceBuffer.appendBuffer(data);
                if (
                    sourceBuffer.buffered.length &&
                    sourceBuffer.buffered.start(0) -
                        sourceBuffer.buffered.end(0) >
                        1
                ) {
                    videoRef.current?.play();
                }
            }
        };

        mediaSource.addEventListener("sourceopen", () => {
            sourceBuffer = mediaSource.addSourceBuffer(
                `video/mp4; codecs="avc1.4d401f`
            );
            // sourceBuffer.addEventListener("updateend", () => {
            //     appendBuffer();
            // });
        });

        return () => {
            if (videoRef.current) videoRef.current.src = "";
            socket.close();
        };
    }, [id, videoRef, websocketURL.href]);

    return (
        <Container maxWidth="md" style={{ width: "100%", marginTop: "20%" }}>
            <Grid container>
                <Grid xs={12}>
                    <Box
                        justifyContent="center"
                        alignItems="center"
                        width="100%"
                        display="flex"
                        paddingY={2}
                    >
                        {thisCamera ? (
                            <Typography variant="h3">
                                {thisCamera?.name}
                            </Typography>
                        ) : (
                            <></>
                        )}
                    </Box>
                </Grid>
                <Grid xs={12}>
                    <Paper>
                        <video
                            ref={videoRef}
                            style={{
                                width: `calc(100% - 4rem)`,
                                padding: "2rem",
                            }}
                            controls
                            muted
                            autoPlay
                        />
                    </Paper>
                </Grid>
            </Grid>
            <Link href="/">
                <Fab
                    color="primary"
                    sx={{ position: "absolute", bottom: "2rem", right: "2rem" }}
                >
                    <HomeIcon />
                </Fab>
            </Link>
        </Container>
    );
};

export default CameraViewPage;
