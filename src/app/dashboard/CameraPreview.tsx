import {
    Card,
    CardContent,
    CardHeader,
    CardMedia,
    Paper,
    Skeleton,
    Typography,
} from "@mui/material";
import { RTSPCameraSource } from "@prisma/client";
import axios from "axios";
import Link from "next/link";
import { useEffect, useState } from "react";

interface CameraPreviewProps {
    camera: RTSPCameraSource;
}

export const CameraPreview: React.FC<CameraPreviewProps> = ({ camera }) => {
    const cameraName = camera.name || "Unnamed Camera";
    const [imageURL, setImageURL] = useState("");

    useEffect(() => {
        let url = "";
        (async () => {
            const resp = await axios.get(`/api/cameras/${camera.id}/preview`, {
                responseType: "blob",
            });
            url = URL.createObjectURL(resp.data);
            setImageURL(url);
        })();

        return () => {
            URL.revokeObjectURL(url);
        };
    }, [camera.id]);

    return (
        <Link href={`/camera/${camera.id}`} style={{ textDecoration: "none" }}>
            <Card>
                <CardHeader title={cameraName} subheader={camera.protocol} />
                {!imageURL ? (
                    <Skeleton
                        sx={{ height: 190 }}
                        animation="wave"
                        variant="rectangular"
                    />
                ) : (
                    <CardMedia
                        sx={{ height: 190 }}
                        title={`${cameraName} Preview`}
                        image={imageURL}
                    />
                )}

                <CardContent>
                    <Typography variant="caption">{camera.id}</Typography>
                </CardContent>
            </Card>
        </Link>
    );
};
