"use client";
import { Detail } from "@/utils";
import {
    Dialog,
    DialogTitle,
    DialogContent,
    DialogContentText,
    TextField,
    DialogActions,
    Button,
    FormControl,
    FormHelperText,
    InputLabel,
    MenuItem,
    Select,
    Stepper,
    Step,
    StepLabel,
    Typography,
} from "@mui/material";
import axios, { Axios, AxiosError } from "axios";
import * as React from "react";
import { useState } from "react";

interface NewCameraPromptProps {
    open: boolean;
    setOpen: (o: boolean) => any | void;
}
const NewCameraPrompt: React.FC<NewCameraPromptProps> = ({ open, setOpen }) => {
    const [RTSPURL, setRTSPURL] = useState("");
    const [protocol, setProtocol] = useState("tcp");
    const [activeStep, setActiveStep] = useState(0);
    const [activeStepErrorMessage, setActiveStepErrorMessage] = useState("");
    const [detailsArray, setDetailsArray] = useState<Detail[]>([]);
    const [selectedVideoChannel, setSelectedVideoChannel] = useState(0);

    const handleClose = () => {
        setOpen(false);
    };

    const advanceStep = () => {
        setActiveStep((s) => s + 1);
        setActiveStepErrorMessage("");
    };

    const handleBasicInformationNext = async () => {
        const data = { url: RTSPURL, protocol };
        try {
            const resp = await axios.post("/api/cameras/get-camera-info", data);
            setDetailsArray(resp.data);
            console.log(resp.data);
            advanceStep();
        } catch (e: any | AxiosError) {
            setActiveStepErrorMessage("Server Error");
            if (axios.isAxiosError(e)) {
                if (e.response) {
                    setActiveStepErrorMessage(e.response.data.error);
                }
            }
            console.error(e);
        }
    };

    const handleChooseChannelComplete = async () => {
        const data = {
            url: RTSPURL,
            protocol,
            control: detailsArray[selectedVideoChannel].mediaSource.control,
        };
        try {
            const resp = await axios.post("/api/cameras/add-camera", data);
            console.log(resp.data);
            setOpen(false);
        } catch (e: any | AxiosError) {
            setActiveStepErrorMessage("Server Error");
            if (axios.isAxiosError(e)) {
                if (e.response) {
                    setActiveStepErrorMessage(e.response.data.error);
                }
            }
            console.error(e);
        }
    };

    const basicInformation = (
        <>
            <DialogContent>
                <DialogContentText>
                    To add a new camera, please enter the following information.
                </DialogContentText>
                <TextField
                    autoFocus
                    margin="dense"
                    label="RTSP URL"
                    type="text"
                    fullWidth
                    variant="standard"
                    required={true}
                    value={RTSPURL}
                    onChange={(e) => setRTSPURL(e.target.value)}
                />
                <FormControl
                    sx={{ mt: 1 }}
                    margin="dense"
                    size="small"
                    variant="standard"
                    required={true}
                >
                    <InputLabel>Protocol</InputLabel>
                    <Select
                        value={protocol}
                        label="Protocol"
                        onChange={(e) => setProtocol(e.target.value)}
                    >
                        <MenuItem value="tcp">TCP</MenuItem>
                        <MenuItem value="udp">UDP</MenuItem>
                    </Select>
                    <FormHelperText>If unsure, try each.</FormHelperText>
                </FormControl>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>Cancel</Button>
                <Button onClick={handleBasicInformationNext}>Next</Button>
            </DialogActions>
        </>
    );

    const chooseChannel = (
        <>
            <DialogContent>
                <DialogContentText>
                    Select the correct video channel to monitor
                </DialogContentText>
                <FormControl
                    sx={{ mt: 1 }}
                    margin="dense"
                    size="medium"
                    variant="standard"
                    required={true}
                    fullWidth={true}
                >
                    <InputLabel>Video Channel</InputLabel>
                    <Select
                        value={selectedVideoChannel}
                        label="Video Channel"
                        onChange={(e) =>
                            setSelectedVideoChannel(e.target.value as number)
                        }
                    >
                        {detailsArray.map((detail, i) => (
                            <MenuItem value={i} key={i}>
                                {i}. {detail.codec} {detail.mediaSource.type}
                            </MenuItem>
                        ))}
                    </Select>
                    <FormHelperText>If unsure, try each.</FormHelperText>
                </FormControl>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>Cancel</Button>
                <Button onClick={handleChooseChannelComplete}>Done</Button>
            </DialogActions>
        </>
    );

    const steps = [
        { name: "Basic Information", content: basicInformation },
        {
            name: "Choose Channel",
            content: chooseChannel,
        },
    ];

    return (
        <Dialog open={open} onClose={handleClose}>
            <DialogTitle>New Camera Setup</DialogTitle>
            <Stepper activeStep={activeStep} alternativeLabel>
                {steps.map((step, i) => (
                    <Step key={step.name} completed={activeStep > i}>
                        <StepLabel
                            error={
                                activeStep === i && activeStepErrorMessage != ""
                            }
                            optional={
                                activeStep === i &&
                                activeStepErrorMessage && (
                                    <Typography variant="caption" color="error">
                                        {activeStepErrorMessage}
                                    </Typography>
                                )
                            }
                        >
                            {step.name}
                        </StepLabel>
                    </Step>
                ))}
            </Stepper>
            {steps[activeStep].content}
        </Dialog>
    );
};

export default NewCameraPrompt;
