"use client";
import { Container, Box, Fab } from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2/Grid2";
import AddIcon from "@mui/icons-material/Add";
import { useState } from "react";
import NewCameraPrompt from "./NewCameraPrompt";
import { useCameras } from "@/client-utils";
import { CameraPreview } from "./CameraPreview";

const DashboardPage = () => {
    const { cameras, isLoading, isError } = useCameras();
    // console.log(cameras);

    const [newCameraPromptOpen, setNewCameraPromptOpen] = useState(false);
    return (
        <Container style={{ width: "100%", height: "100%" }} maxWidth="xl">
            <Grid container sx={{ mt: 2 }} spacing={2}>
                {isError && "Error"}
                {!isLoading &&
                    cameras.map((camera) => {
                        return (
                            <Grid key={camera.id} xs={12} md={6} lg={4} xl={3}>
                                <CameraPreview camera={camera} />
                            </Grid>
                        );
                    })}
            </Grid>
            <Fab
                color="primary"
                sx={{ position: "absolute", bottom: "2rem", right: "2rem" }}
                onClick={() => setNewCameraPromptOpen(true)}
            >
                <AddIcon />
            </Fab>
            <NewCameraPrompt
                open={newCameraPromptOpen}
                setOpen={setNewCameraPromptOpen}
            />
        </Container>
    );
};

export default DashboardPage;
