import './globals.css'
import type { Metadata } from 'next'
import { Inter } from 'next/font/google'

export const metadata: Metadata = {
  title: 'Tamra Station',
  description: 'Save, View, Connect Camera Feeds',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body>
        <div style={{height: '100%', width: '100%'}}>{children}</div>
      </body>
    </html>
  )
}
