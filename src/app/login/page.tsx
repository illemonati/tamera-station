"use client";
import {
    Alert,
    Box,
    Container,
    Fab,
    Paper,
    Snackbar,
    TextField,
    Typography,
    useMediaQuery,
    useTheme,
} from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";
import LoginIcon from "@mui/icons-material/Login";
import axios from "axios";
import { useEffect, useState } from "react";
import { useRouter } from "next/navigation";
import Cookies from "js-cookie";

const LoginPage = () => {
    const [password, setPassword] = useState("");
    const [loginError, setLoginError] = useState(false);
    const router = useRouter();
    const theme = useTheme();
    const lessThenMD = useMediaQuery(theme.breakpoints.down("md"));

    useEffect(() => {
        console.log(password);
        // document.cookie = `password=${password}; Path="/"`;
        Cookies.set("password", password, { path: "/" });
    }, [password]);

    const tryLogin = async () => {
        const res = await axios.post("/api/login");
        const { valid } = res.data;

        if (valid) {
            const prevPage = Cookies.get("prev-page");
            if (prevPage) {
                Cookies.remove("prev-page");
                router.push(prevPage);
            } else {
                router.push("/");
            }
        } else {
            setLoginError(true);
        }
    };

    return (
        <Container style={{ width: "100%", height: "100%" }}>
            <Box
                display="flex"
                justifyContent="center"
                alignItems="center"
                width="100%"
                height="100%"
                marginTop="-2rem"
                flexDirection="column"
                gap={5}
            >
                <Typography sx={{ textAlign: "center" }} variant="h4">
                    Welcome To Tamra Station
                </Typography>
                <Container maxWidth="sm">
                    <Box width="100%">
                        <Paper
                            variant="outlined"
                            sx={{ background: "rgba(0,0,0,0)" }}
                        >
                            <Container>
                                <Box paddingY="1rem">
                                    <Grid container spacing={2}>
                                        <Grid md={10.5} xs={12}>
                                            <TextField
                                                label="Password"
                                                type="password"
                                                variant="outlined"
                                                value={password}
                                                onChange={(e) =>
                                                    setPassword(e.target.value)
                                                }
                                                onKeyDown={(e) =>
                                                    (e.key === "Enter" ||
                                                        e.keyCode === 13) &&
                                                    tryLogin()
                                                }
                                                fullWidth
                                            />
                                        </Grid>
                                        <Grid
                                            md={1.5}
                                            xs={12}
                                            textAlign="center"
                                        >
                                            <Fab
                                                color="primary"
                                                aria-label="add"
                                                onClick={() => tryLogin()}
                                                variant={
                                                    lessThenMD
                                                        ? "extended"
                                                        : "circular"
                                                }
                                            >
                                                <LoginIcon
                                                    sx={{
                                                        mr: { xs: 1, md: 0 },
                                                    }}
                                                />
                                                {lessThenMD && "Login"}
                                            </Fab>
                                        </Grid>
                                    </Grid>
                                </Box>
                            </Container>
                        </Paper>
                    </Box>
                </Container>
                <Snackbar
                    open={loginError}
                    onClose={() => setLoginError(false)}
                    autoHideDuration={3000}
                >
                    <Alert severity="error" sx={{ width: "100%" }}>
                        Invalid Password
                    </Alert>
                </Snackbar>
            </Box>
        </Container>
    );
};

export default LoginPage;
