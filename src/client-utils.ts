"use client";
import { RTSPCameraSource } from "@prisma/client";
import axios from "axios";
import useSWR from "swr";

export const fetcher = (url: string) => axios.get(url).then((res) => res.data);

export const useCameras = () => {
    const { data, error, isLoading } = useSWR(`/api/cameras`, fetcher);

    return {
        cameras: data as RTSPCameraSource[],
        isLoading,
        isError: error,
    };
};
