export const TAMRA_PASSWORD = process.env.TAMRA_PASSWORD;
export const PORT = 19927;
export const DB_PATH = "./dev-db.db";
export const UNITS_BEFORE_SEND = 1;
