import { NextRequest, NextResponse } from "next/server";
import { TAMRA_PASSWORD } from "./config";

export const middleware = (req: NextRequest) => {
    const cookiePassword = req.cookies.get("password")?.value;
    const path = req.nextUrl.pathname.toLowerCase();

    if (cookiePassword !== TAMRA_PASSWORD) {
        if (path.startsWith("/api/")) {
            if (!path.startsWith("/api/login")) {
                return NextResponse.json("unauthorized", { status: 401 });
            }
        } else {
            const res = NextResponse.redirect(new URL("/login", req.url));
            res.cookies.delete("password");
            res.cookies.set("prev-page", req.nextUrl.pathname, { path: "/" });
            return res;
        }
    }

    return NextResponse.next();
};

export const config = {
    matcher: [
        /*
         * Match all request paths except for the ones starting with:
         * - _next/static (static files)
         * - _next/image (image optimization files)
         * - favicon.ico (favicon file)
         * - login page
         */
        "/((?!_next/static|_next/image|favicon.ico|login).*)",
    ],
};
