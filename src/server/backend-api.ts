import express from "express";
import { testReceive } from "../stream-receive";
import bodyParser from "body-parser";
import liveStream from "./live-stream";
import cookieParser from "cookie-parser";
import { TAMRA_PASSWORD } from "../config";
import expressWs from "express-ws";

const SimpleAuthMiddleware = (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
) => {
    const { password } = req.cookies;
    if (password !== TAMRA_PASSWORD) {
        return res.status(401).json("unauthorized");
    }
    next();
};

export const backendAPIRouter = express.Router() as expressWs.Router;
backendAPIRouter.use(bodyParser.json());
backendAPIRouter.use(cookieParser());
backendAPIRouter.use(SimpleAuthMiddleware);
backendAPIRouter.use("/live-stream", liveStream);

backendAPIRouter.get("/version", (req, res) => {
    res.json({ version: process.env.npm_package_version });
});

export default backendAPIRouter;
