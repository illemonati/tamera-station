import express from "express";
import expressWs from "express-ws";
import { prisma } from "./prisma";
import { streamCamera } from "../stream-receive";
import JMuxer from "jmuxer";
import { createWebSocketStream } from "ws";
import { RawToChunks } from "../utils";
import { PassThrough } from "stream";

const liveStream = express.Router() as expressWs.Router;

liveStream.get("/stream-raw", async (req, res) => {
    const { id } = req.query;
    if (typeof id !== "string") {
        return res.status(400).json({ error: "Invalid ID" });
    }
    const camera = await prisma.rTSPCameraSource.findUnique({ where: { id } });
    if (!camera) {
        return res.status(400).json({ error: "Camera Not Found" });
    }

    res.writeHead(200, {
        "Content-Type": "application/octet-stream",
    });

    const { client, detail } = await streamCamera(
        camera.url,
        camera.protocol as "tcp" | "udp",
        camera.control,
        res
    );

    res.on("close", () => client.close());
});

liveStream.ws("/stream-ws", async (socket, req) => {
    const { id } = req.query;
    if (typeof id !== "string") {
        return socket.close();
    }
    const camera = await prisma.rTSPCameraSource.findUnique({ where: { id } });
    if (!camera) {
        return socket.close();
    }

    const jmuxer = new JMuxer({
        debug: false,
        mode: "video",
        node: "",
        flushingTime: 300,
    });

    const rawFeedPassThrough = new PassThrough();

    const videoChunkStream = RawToChunks(rawFeedPassThrough);

    const jmuxerStream = jmuxer.createStream();

    const { client } = await streamCamera(
        camera.url,
        camera.protocol as "tcp" | "udp",
        camera.control,
        rawFeedPassThrough
    );

    const websocketStream = createWebSocketStream(socket);

    videoChunkStream.pipe(jmuxerStream).pipe(websocketStream);

    socket.on("error", () => {
        console.log("socket error");
        client.close();
    });

    socket.on("close", () => client.close());
});

export default liveStream;
