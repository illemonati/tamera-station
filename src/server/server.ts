import express from "express";
import expressWs from "express-ws";
import next from "next";
import dotenv from "dotenv";
import { testReceive } from "../stream-receive";
import { DB_PATH, PORT } from "../config";

dotenv.config({
    path: ".env",
});

const isDev = process.env.NODE_ENV !== "production";

const server = expressWs(express()).app;

const setupServer = async () => {
    // this is required after to define the ws
    const { backendAPIRouter } = await import("./backend-api");
    server.use("/bapi", backendAPIRouter);
};

const setupNext = async () => {
    const nextApp = next({ dev: isDev, port: PORT });
    const nextHandler = nextApp.getRequestHandler();
    await nextApp.prepare();
    server.get("*", (req, res) => {
        return nextHandler(req, res);
    });
    server.post("*", (req, res) => {
        return nextHandler(req, res);
    });
};

const setupDB = async () => {
    // await new Promise<void>((r, e) => {
    //     new sqlite3.Database(DB_PATH, sqlite3.OPEN_CREATE | sqlite3.OPEN_READWRITE, (err) => {
    //         err ? e(err) : r();
    //     })}
    // );
};

const main = async () => {
    await Promise.all([setupServer(), setupNext(), setupDB()]);

    const serverInstance = server.listen(PORT);
    console.log(
        `Tamra Station Started: ${JSON.stringify(serverInstance.address())}`
    );
    if (isDev) {
        testReceive().then();
    }
};

main().then();
