import { H264Transport, H265Transport, RTSPClient } from "yellowstone";
import { prisma } from "./server/prisma";
import tmp from "tmp";
import fs from "fs";
import { exec } from "child_process";
import util from "util";
import internal, { PassThrough } from "stream";
import { RawToChunks } from "./utils";

const execPromise = util.promisify(exec);

export const connectToRTSPEndPoint = async (
    url: string,
    protocol: "tcp" | "udp"
) => {
    const client = new RTSPClient("", "");
    const detailsArray = await client.connect(url, {
        connection: protocol,
        keepAlive: true,
    });
    return { client, detailsArray };
};

export const addCameraToDB = async (
    url: string,
    protocol: "tcp" | "udp",
    control: string
) => {
    return await prisma.rTSPCameraSource.create({
        data: {
            url,
            protocol,
            control,
        },
    });
};

export const getCameraDetail = async (
    url: string,
    protocol: "tcp" | "udp",
    control: string
) => {
    const { client, detailsArray } = await connectToRTSPEndPoint(url, protocol);
    for (const detail of detailsArray) {
        if (detail.mediaSource.control === control) {
            return { client, detail };
        }
    }
    throw new Error("Unable to match control");
};

export const streamCamera = async (
    url: string,
    protocol: "tcp" | "udp",
    control: string,
    writeStream: internal.Writable
) => {
    const { client, detail } = await getCameraDetail(url, protocol, control);

    let transport = null;
    if (detail.codec === "H264") {
        transport = new H264Transport(client, writeStream, detail);
    } else if (detail.codec == "H265") {
        transport = new H265Transport(client, writeStream, detail);
    } else {
        throw new Error("Unsupported Codec");
    }
    await client.play();

    return { client, detail };
};

export const getPreview = async (
    url: string,
    protocol: "tcp" | "udp",
    control: string
) => {
    const tmpfile = tmp.fileSync({ discardDescriptor: true, postfix: ".h264" });
    const tmpImageFile = tmp.fileSync({
        discardDescriptor: true,
        postfix: ".png",
    });
    const videoFileStream = fs.createWriteStream(tmpfile.name);

    const videoPassthrough = new PassThrough();

    const { client, detail } = await streamCamera(
        url,
        protocol,
        control,
        videoPassthrough
    );

    const chunkExtrator = RawToChunks(videoPassthrough, 3);

    const videoData = await new Promise((r) => {
        const vidBufs: Buffer[] = [];
        chunkExtrator.on("data", ({ video }) => {
            vidBufs.push(video);
            if (vidBufs.length >= 100) {
                videoPassthrough.end();
                client.close().then();
                r(Buffer.concat(vidBufs));
            }
        });
    });

    videoFileStream.write(videoData);
    videoFileStream.close();

    await client.close();

    const { stdout, stderr } = await execPromise(
        `ffmpeg -i ${tmpfile.name} -frames:v 1 -f image2 -y ${tmpImageFile.name}`
    );

    return fs.createReadStream(tmpImageFile.name);
};

export const testReceive = async () => {
    console.log("starting test receive");
    const url = "rtsp://127.0.0.1:8554/mystream.mp4";
    const { client, detailsArray } = await connectToRTSPEndPoint(url, "tcp");
};
