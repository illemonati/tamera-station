import transform from "sdp-transform";
import { prisma } from "./server/prisma";
import { Readable } from "stream";
import { UNITS_BEFORE_SEND } from "./config";

export type Detail = {
    codec: string;
    mediaSource: {
        type: string;
        port: number;
        protocol: string;
        payloads?: string | undefined;
    } & transform.MediaDescription;
    transport: {
        [key: string]: string;
    };
};

export const getRTSPCameaSource = async (id: string) => {
    return prisma.rTSPCameraSource.findUniqueOrThrow({
        where: {
            id,
        },
    });
};

export const RawToChunks = (
    stream: Readable,
    unitsBeforeSend: number = UNITS_BEFORE_SEND
) => {
    const resultStream = new Readable({
        objectMode: true,
        read(size: number) {},
    });

    stream.on("end", () => {
        console.log("end");
    });

    stream.once("readable", async () => {
        let buffer = [] as number[];
        let total = 0;
        let i = 0;
        let zerosRecorded = 0;
        let foundFirstFrame = false;
        let frameStart = 0;
        let resultBuffer = [];
        while (!stream.readableEnded) {
            const readRes = stream.read(1);
            if (readRes == null) {
                try {
                    await new Promise<void>((r, e) => {
                        stream.once("readable", () => r());
                        stream.once("end", () => e());
                        stream.once("error", () => e());
                    });
                } catch (e) {
                    console.log("ending read");
                    return;
                }
                continue;
            }

            if (readRes === undefined) break;
            if (readRes[0] === undefined) break;

            buffer[i] = readRes[0];
            total++;
            if (zerosRecorded >= 2 && buffer[i] === 1) {
                const newFrameStart = i - 3;
                if (foundFirstFrame) {
                    const frameBuffer = Buffer.from(
                        buffer.slice(frameStart, newFrameStart)
                    );
                    resultBuffer.push(frameBuffer);

                    // to prevent choppy video
                    if (resultBuffer.length >= UNITS_BEFORE_SEND) {
                        resultStream.push({
                            video: Buffer.concat(resultBuffer),
                        });
                        resultBuffer = [];
                    }
                    frameStart = 0;
                    buffer = buffer.slice(newFrameStart);
                    i -= newFrameStart;
                }
                foundFirstFrame = true;
                zerosRecorded = 0;
            }

            if (buffer[i] === 0) {
                zerosRecorded += 1;
            } else {
                zerosRecorded = 0;
            }
            i++;
        }
    });
    return resultStream;
};
